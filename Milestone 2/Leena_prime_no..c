#include<stdio.h>

int is_prime(unsigned int);

int main(){

    int number, prime;
    printf("Enter a Positive Number to Check if it is Prime Number or Not: \n");
    scanf("%d",&number);
    if (is_prime(number) == 1)
        printf("%d is a Prime Number",number);
    else
        printf("%d is not a Prime Number",number);
    return 0;
}

int is_prime(unsigned int n){
    int flag = 1;
    for (int i=2;i<=(n/2);i++){
        if (n%i == 0){
            flag=0;
            break;
        }
    }
    return flag;
}
