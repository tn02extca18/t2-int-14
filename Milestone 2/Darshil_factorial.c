#include<stdio.h>

unsigned int fact(unsigned int);

int main(){
    unsigned int number;
    printf("Enter a Positive Number to Get Factorial: \n");
    scanf("%d",&number);
    printf("Factorial of %d is %d",number, fact(number));
    return 0;
}

unsigned int fact(unsigned int n){
    int i, f=1;
    for(i=1; i<=n; i++){
        f = f * i;
    }
    return f;
}
