#include<stdio.h>

double power(double, int);

double FV(double, unsigned int, double);

int main(){
    double r, pv;
    unsigned int n;
    printf("Enter the Present Value, Rate and Period: \n");
    scanf("%lf %lf %d",&pv, &r, &n);
    printf("The Future Value is %.2lf",FV(r, n, pv));
    return 0;
}

double power(double x, int n){
    double res = 1;
    for(int i=n;i>0;i--){
        res = res * x;
    }
    return res;
}

double FV(double rate, unsigned int nperiods, double PV){
    double FV;
    FV = PV * power((1+rate),nperiods);
    return FV;
}
