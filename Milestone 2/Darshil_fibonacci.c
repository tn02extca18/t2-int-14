#include<stdio.h>

int main(){
    int i, num, t1 = 0, t2 = 1, t3;

    printf("Enter the number count for Fibonacci Series: \n");
    scanf("%d", &num);

    printf("Fibonacci Series: ");

    for (i=1; i<=num; ++i)
    {
        printf("%d ", t1);
        t3 = t1 + t2;
        t1 = t2;
        t2 = t3;
    }
    return 0;
}
