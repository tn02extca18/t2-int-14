#include<stdio.h>

int lcm(int, int);

int main(){
    int a, b, c;
    printf("Enter 3 Positive Numbers for LCM: \n");
    scanf("%d %d %d",&a, &b, &c);
    printf("LCM of the numbers is %d", lcm(a,lcm(b,c)));
    return 0;
}

int lcm(int num1, int num2){
    int bigNum;
    bigNum = (num1 > num2)? num1 : num2 ;
    while(1){
    if( (bigNum%num1)==0 && (bigNum%num2)==0 )
        break;
    else
        bigNum++;
    }
    return bigNum;
}
