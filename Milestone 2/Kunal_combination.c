#include<stdio.h>

unsigned int fact(unsigned int);
unsigned int nPr(unsigned int, unsigned int);
unsigned int nCr(unsigned int, unsigned int);

int main(){
    unsigned int n, r;
    printf("Enter the value of 'n' & 'r' for Combinations (nCr): \n");
    scanf("%d %d",&n, &r);
    printf("Value of Combinations is %d",nCr(nPr(n,r),r));
    return 0;
}

unsigned int fact(unsigned int n){
    int i, f=1;
    for(i=1; i<=n; i++){
        f = f * i;
    }
    return f;
}

unsigned int nPr(unsigned int n, unsigned int r){
    unsigned int perm;
    perm = fact(n) / fact(n-r);
    return perm;
}

unsigned int nCr(unsigned int p, unsigned int r){
    unsigned int comb;
    comb = p / fact(r);
    return comb;
}
