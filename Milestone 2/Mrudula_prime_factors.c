#include <stdio.h>

int is_prime(unsigned int);

int main(){
    unsigned int number, f;

    printf("Enter a Positive Number to Find Prime Factors: \n");
    scanf("%d", &number);

    printf("Prime Factors of %d are: ", number);
    for(f=1; f<=number; f++){
        if (number%f == 0){
            if(is_prime(f)==1)
                printf("%d, ",f);
        }
    }
    return 0;
}

int is_prime(unsigned int n){
    int flag = 1;
    for (int i=2;i<=(n/2);i++){
        if (n%i == 0){
            flag=0;
            break;
        }
    }
    return flag;
}
