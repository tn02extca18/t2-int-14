#include<stdio.h>

int main(){
    int a[50], n, search, i, j, flag, index;
    printf("Enter Array Size: \n");
    scanf("%d",&n);
    printf("Enter the Numbers: \n");
    for(int i=0; i<n; i++){
        scanf("%d",&a[i]);
    }
    printf("Enter the Number you want to search in Array: \n");
    scanf("%d", &search);
    for(int i=0; i<n; i++){
        if(search == a[i]){
            flag=1;
            index=i;
            break;
        }
        else
            flag=0;
    }
    if (flag == 1){
        printf("Number %d exists in the Array at index %d", search, index);
    }
    else
        printf("Number %d does not exist in the Array",search);
    return 0;
}
