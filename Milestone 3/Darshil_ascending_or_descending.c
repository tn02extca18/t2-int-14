#include<stdio.h>

int main(){
    int a[50], n, i, j, temp, sort;
    printf("Enter Array Size: \n");
    scanf("%d",&n);
    printf("Enter the Numbers: \n");
    for(int i=0; i<n; i++){
        scanf("%d",&a[i]);
    }
    for(int flag=0; flag!=1;){
    printf("Enter '0' to sort Array in Ascending Order or '1' to sort in Descending Order : (0/1) \n");
    scanf("%d", &sort);
    if(sort == 0){
        for(i=0; i<n; i++){
            for(j=i+1; j<n; j++){
                if (a[i]>a[j]){
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        printf("Array in Ascending Order is as follows : \n");
        for(int i=0; i<n; i++){
        printf("%d ",a[i]);
        }
        flag = 1;
    }
    else if (sort == 1){
        for(i=0; i<n; i++){
            for(j=i+1; j<n; j++){
                if (a[i]<a[j]){
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        printf("Array in Descending Order is as follows : \n");
        for(int i=0; i<n; i++){
        printf("%d ",a[i]);
        }
        flag = 1;
    }
    else
        printf("Error! Please try again. \n");
    }
    return 0;
}
